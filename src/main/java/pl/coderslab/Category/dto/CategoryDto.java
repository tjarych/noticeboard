package pl.coderslab.Category.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryDto {

	private Long id;

	@NotBlank
	private String name;

	private Boolean isActice;

}
