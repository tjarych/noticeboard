package pl.coderslab.Category.Service;

import pl.coderslab.Category.dto.CategoryDto;
import pl.coderslab.Commons.Service.BaseCrudService;

public interface CategoryService extends BaseCrudService<CategoryDto, Long> {

}
