package pl.coderslab.Category.Service.Implementation;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.coderslab.Category.Repository.CategoryRepository;
import pl.coderslab.Category.Service.CategoryService;
import pl.coderslab.Category.domain.Category;
import pl.coderslab.Category.dto.CategoryDto;
import pl.coderslab.Commons.DtoAndEntityConverter.DtoAndEntityConverter;

@Service
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository categoryRepository;
	private final DtoAndEntityConverter converter;

	@Autowired
	public CategoryServiceImpl(CategoryRepository categoryRepository,
			DtoAndEntityConverter converter) {
		this.categoryRepository = categoryRepository;
		this.converter = converter;
	}

	@Override
	public CategoryDto findById(Long id) {
		return converter.toCategoryDto(categoryRepository.getOne(id));
	}

	@Override
	public CategoryDto save(CategoryDto dto) {
		return converter.toCategoryDto(
				categoryRepository.save(converter.toCategoryEntity(dto)));
	}

	@Override
	public void deleteFromDb(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public List<CategoryDto> getAll() {
		return toCategoryDtoList(categoryRepository.findAll());
	}

	private List<CategoryDto> toCategoryDtoList(List<Category> list) {
		return list.stream().filter(Objects::nonNull)
				.map(converter::toCategoryDto).collect(Collectors.toList());
	}

}
