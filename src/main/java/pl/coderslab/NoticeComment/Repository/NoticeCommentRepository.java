package pl.coderslab.NoticeComment.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.coderslab.Notice.domain.Notice;
import pl.coderslab.NoticeComment.domain.NoticeComment;
import pl.coderslab.User.Domain.User;

@Repository
public interface NoticeCommentRepository
		extends
			JpaRepository<NoticeComment, Long> {
	
	List<NoticeComment> findAllByAuthorIdOrderByCreatedDesc(Long id);
	List<NoticeComment> findAllByAuthorOrderByCreatedDesc(User author);
	
	
	List<NoticeComment> findAllByNoticeIdOrderByCreatedDesc(Long id);
	List<NoticeComment> findAllByNoticeOrderByCreatedDesc(Notice notice);
}
