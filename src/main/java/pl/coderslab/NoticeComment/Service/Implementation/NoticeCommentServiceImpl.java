package pl.coderslab.NoticeComment.Service.Implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.coderslab.Commons.DtoAndEntityConverter.DtoAndEntityConverter;
import pl.coderslab.Notice.dto.NoticeDto;
import pl.coderslab.NoticeComment.Repository.NoticeCommentRepository;
import pl.coderslab.NoticeComment.Service.NoticeCommentService;
import pl.coderslab.NoticeComment.domain.NoticeComment;
import pl.coderslab.NoticeComment.dto.NoticeCommentDto;
import pl.coderslab.User.Dto.UserDto;

@Service
public class NoticeCommentServiceImpl implements NoticeCommentService {

	private final NoticeCommentRepository commentRepository;
	private final DtoAndEntityConverter converter;

	@Autowired
	public NoticeCommentServiceImpl(NoticeCommentRepository commentRepository,
			DtoAndEntityConverter converter) {
		this.commentRepository = commentRepository;
		this.converter = converter;
	}

	@Override
	public NoticeCommentDto findById(Long id) {
		return converter.toNoticeCommentDto(commentRepository.getOne(id));
	}

	@Override
	public NoticeCommentDto save(NoticeCommentDto dto) {
		return converter.toNoticeCommentDto(
				commentRepository.save(converter.toNoticeCommentEntity(dto)));
	}

	@Override
	public void deleteFromDb(Long id) {
		commentRepository.deleteById(id);
	}

	@Override
	public List<NoticeCommentDto> getAll() {
		return toNoticeCommentDtoList(commentRepository.findAll());
	}

	@Override
	public List<NoticeCommentDto> findAllByAuthorIdOrderByCreatedDesc(Long id) {
		return toNoticeCommentDtoList(
				commentRepository.findAllByAuthorIdOrderByCreatedDesc(id));
	}

	@Override
	public List<NoticeCommentDto> findAllByAuthorOrderByCreatedDesc(
			UserDto author) {
		return toNoticeCommentDtoList(
				commentRepository.findAllByAuthorOrderByCreatedDesc(
						converter.toUserEntity(author)));
	}

	@Override
	public List<NoticeCommentDto> findAllByNoticeIdOrderByCreatedDesc(Long id) {
		return toNoticeCommentDtoList(
				commentRepository.findAllByNoticeIdOrderByCreatedDesc(id));
	}

	@Override
	public List<NoticeCommentDto> findAllByNoticeOrderByCreatedDesc(
			NoticeDto notice) {
		return toNoticeCommentDtoList(
				commentRepository.findAllByNoticeOrderByCreatedDesc(
						converter.toNoticeEntity(notice)));
	}

	private List<NoticeCommentDto> toNoticeCommentDtoList(
			List<NoticeComment> list) {
		return list.stream().map(converter::toNoticeCommentDto)
				.collect(Collectors.toList());
	}
}
