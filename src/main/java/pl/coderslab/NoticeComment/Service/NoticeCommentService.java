package pl.coderslab.NoticeComment.Service;

import java.util.List;

import pl.coderslab.Commons.Service.BaseCrudService;
import pl.coderslab.Notice.dto.NoticeDto;
import pl.coderslab.NoticeComment.dto.NoticeCommentDto;
import pl.coderslab.User.Dto.UserDto;

public interface NoticeCommentService extends BaseCrudService<NoticeCommentDto, Long> {
	
	List<NoticeCommentDto> findAllByAuthorIdOrderByCreatedDesc(Long id);
	List<NoticeCommentDto> findAllByAuthorOrderByCreatedDesc(UserDto author);
	
	
	List<NoticeCommentDto> findAllByNoticeIdOrderByCreatedDesc(Long id);
	List<NoticeCommentDto> findAllByNoticeOrderByCreatedDesc(NoticeDto notice);

}
