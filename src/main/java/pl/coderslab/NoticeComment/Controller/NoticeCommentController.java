package pl.coderslab.NoticeComment.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.coderslab.Commons.ErrorsUtil.ErrorsUtil;
import pl.coderslab.Commons.Result.Result;
import pl.coderslab.NoticeComment.Service.NoticeCommentService;
import pl.coderslab.NoticeComment.dto.NoticeCommentDto;

@RestController
@RequestMapping("/noticeComment")
public class NoticeCommentController {

	private final NoticeCommentService commentService;

	@Autowired
	public NoticeCommentController(NoticeCommentService commentService) {
		this.commentService = commentService;
	}

	@GetMapping(path = "/byNoticeId/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result getCommentsByNoticeId(@PathVariable("id") Long id) {
		return Result
				.ok(commentService.findAllByNoticeIdOrderByCreatedDesc(id));
	}

	@GetMapping(path = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result getCommentsById(@PathVariable("id") Long id) {
		return Result.ok(commentService.findById(id));
	}

	@GetMapping(path = "/byUserId/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result getCommentsByUserId(@PathVariable("id") Long id) {
		return Result
				.ok(commentService.findAllByAuthorIdOrderByCreatedDesc(id));
	}

	@PostMapping(produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
	public Result addNewComment(@Valid @RequestBody NoticeCommentDto dto,
			BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
						bindingResult.getFieldErrors()), dto);
			}
			return Result.ok(commentService.save(dto));

		} catch (EntityNotFoundException e) {
			return Result.error("Noticie with given ID dosen't exist");
		}
	}

	@PutMapping(produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
	public Result editComment(@Valid @RequestBody NoticeCommentDto dto,
			BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
						bindingResult.getFieldErrors()), dto);
			}
			return Result.ok(commentService.save(dto));

		} catch (EntityNotFoundException e) {
			return Result.error("Noticie with given ID dosen't exist");
		}
	}

}
