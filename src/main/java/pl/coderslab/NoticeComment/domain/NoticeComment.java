package pl.coderslab.NoticeComment.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.coderslab.Notice.domain.Notice;
import pl.coderslab.User.Domain.User;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "comments")
public class NoticeComment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String content;

	private LocalDateTime created = LocalDateTime.now();

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User author;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="notice_id")
	private Notice notice;
}
