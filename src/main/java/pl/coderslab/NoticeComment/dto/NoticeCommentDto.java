package pl.coderslab.NoticeComment.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.coderslab.Notice.dto.NoticeDto;
import pl.coderslab.User.Dto.UserDto;

@Getter
@Setter
@NoArgsConstructor
public class NoticeCommentDto {

	private Long id;
	private String content;
	private LocalDateTime created = LocalDateTime.now();
	private UserDto author;
	private NoticeDto notice;
}
