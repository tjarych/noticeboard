package pl.coderslab.Notice.Controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.coderslab.Commons.ErrorsUtil.ErrorsUtil;
import pl.coderslab.Commons.Result.Result;
import pl.coderslab.Notice.Service.NoticeService;
import pl.coderslab.Notice.dto.NoticeDto;
import pl.coderslab.User.Service.UserService;

@RestController
@RequestMapping("/notice")
public class NoticeController {

	private final UserService userService;
	private final NoticeService noticeService;

	@Autowired
	public NoticeController(UserService userService,
			NoticeService noticeService) {
		this.userService = userService;
		this.noticeService = noticeService;
	}

	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getAllNotices() {
		return Result.ok(noticeService.getAll());
	}

	@GetMapping(path = "/allActiveNotices", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getAllActiveNotices() {
		return Result.ok(noticeService.findAllByIsActive(true));
	}

	@GetMapping(path = "/allInActiveNotices", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getAllInActiveNotices() {
		return Result.ok(noticeService.findAllByIsActive(false));
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getNoticeById(@PathVariable("id") Long id) {
		return Result.ok(noticeService.findById(id));
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result addNewNotice(@Valid @RequestBody NoticeDto dto,
			BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
						bindingResult.getFieldErrors()), dto);
			}
			return Result.ok(noticeService.save(dto));

		} catch (EntityNotFoundException e) {
			return Result.error("Noticie with given ID dosen't exist");
		}
	}

	@PutMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result editNotice(@Valid @RequestBody NoticeDto dto,
			BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
						bindingResult.getFieldErrors()), dto);
			}
			return Result.ok(noticeService.save(dto));

		} catch (EntityNotFoundException e) {
			return Result.error("Noticie with given ID dosen't exist");
		}

	}

	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result deleteNotice(@PathVariable("id") Long id) {
		try {
			noticeService.deleteFromDb(id);
			return Result.ok("Notice has been deleted");
		} catch (Exception e) {
			return Result.error("Cannot delete notice of given ID");
		}

	}

}
