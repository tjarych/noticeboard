package pl.coderslab.Notice.Service;

import java.util.List;

import pl.coderslab.Commons.Service.BaseCrudService;
import pl.coderslab.Notice.dto.NoticeDto;

public interface NoticeService extends BaseCrudService<NoticeDto, Long> {

	public List<NoticeDto> findAllByIsActive(Boolean isActive);
}
