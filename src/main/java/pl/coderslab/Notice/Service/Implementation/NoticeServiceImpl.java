package pl.coderslab.Notice.Service.Implementation;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.coderslab.Commons.DtoAndEntityConverter.DtoAndEntityConverter;
import pl.coderslab.Notice.Repository.NoticeRepository;
import pl.coderslab.Notice.Service.NoticeService;
import pl.coderslab.Notice.domain.Notice;
import pl.coderslab.Notice.dto.NoticeDto;

@Service
public class NoticeServiceImpl implements NoticeService {

	private final NoticeRepository noticeRepository;
	private final DtoAndEntityConverter converter;

	@Autowired
	public NoticeServiceImpl(NoticeRepository noticeRepository,
			DtoAndEntityConverter converter) {
		this.noticeRepository = noticeRepository;
		this.converter = converter;
	}

	@Override
	public NoticeDto findById(Long id) {
		return converter.toNoticeDto(noticeRepository.getOne(id));
	}

	@Override
	public NoticeDto save(NoticeDto dto) throws EntityNotFoundException {
		return converter.toNoticeDto(
				noticeRepository.save(converter.toNoticeEntity(dto)));
	}

	@Override
	public void deleteFromDb(Long id) {
		noticeRepository.deleteById(id);
	}

	@Override
	public List<NoticeDto> getAll() {
		return toNoticeDtoCollection(noticeRepository.findAll());
	}

	@Override
	public List<NoticeDto> findAllByIsActive(Boolean isActive) {
		return toNoticeDtoCollection(
				noticeRepository.findAllByIsActive(isActive));
	}

	private List<NoticeDto> toNoticeDtoCollection(List<Notice> list) {
		return list.stream().map(converter::toNoticeDto)
				.collect(Collectors.toList());

	}

}
