package pl.coderslab.Notice.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.coderslab.Notice.domain.Notice;

@Repository
public interface NoticeRepository extends JpaRepository<Notice, Long> {

	List<Notice> findAllByIsActive(Boolean isActive);

}
