package pl.coderslab.Notice.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.coderslab.Category.dto.CategoryDto;
import pl.coderslab.User.Dto.UserDto;

@Setter
@Getter
@NoArgsConstructor
public class NoticeDto {

	private Long id;

	@NotBlank
	private String subject;

	@NotBlank
	private String content;

	private UserDto owner;

	private LocalDateTime created = LocalDateTime.now();

	private Boolean isActive;

	private Set<CategoryDto> categories = new HashSet<>();

}
