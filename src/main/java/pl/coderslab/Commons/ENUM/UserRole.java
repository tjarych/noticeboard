package pl.coderslab.Commons.ENUM;

public enum UserRole {

	ADMIN,

	USER,

	GUEST
}
