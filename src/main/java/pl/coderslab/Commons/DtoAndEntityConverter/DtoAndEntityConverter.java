package pl.coderslab.Commons.DtoAndEntityConverter;

import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.coderslab.Category.Repository.CategoryRepository;
import pl.coderslab.Category.domain.Category;
import pl.coderslab.Category.dto.CategoryDto;
import pl.coderslab.Image.domain.Image;
import pl.coderslab.Image.dto.ImageDto;
import pl.coderslab.Notice.Repository.NoticeRepository;
import pl.coderslab.Notice.domain.Notice;
import pl.coderslab.Notice.dto.NoticeDto;
import pl.coderslab.NoticeComment.Repository.NoticeCommentRepository;
import pl.coderslab.NoticeComment.domain.NoticeComment;
import pl.coderslab.NoticeComment.dto.NoticeCommentDto;
import pl.coderslab.User.Domain.User;
import pl.coderslab.User.Dto.UserDto;
import pl.coderslab.User.Repository.UserRepository;

@Component
public class DtoAndEntityConverter {

	private final UserRepository userRepository;
	private final NoticeRepository noticeRepository;
	private final CategoryRepository categoryRepository;
	private final NoticeCommentRepository noticeCommentRepository;

	@Autowired
	public DtoAndEntityConverter(UserRepository userRepository,
			NoticeRepository noticeRepository,
			CategoryRepository categoryRepository,
			NoticeCommentRepository noticeCommentRepository) {
		this.userRepository = userRepository;
		this.noticeRepository = noticeRepository;
		this.categoryRepository = categoryRepository;
		this.noticeCommentRepository = noticeCommentRepository;
	}

	public UserDto toUserDto(User user) {
		UserDto dto = new UserDto();

		dto.setId(user.getId());
		dto.setLogin(user.getLogin());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setUserRole(user.getUserRole());
		dto.setEmail(user.getEmail());

		if (Objects.nonNull(user.getNotices())
				&& !user.getNotices().isEmpty()) {
			dto.getNotices().clear();
			user.getNotices().stream().filter(Objects::nonNull)
					.map(this::toNoticeDto)
					.forEachOrdered(el -> dto.getNotices().add(el));
		}

		return dto;
	}

	public UserDto toSimpleUserDto(User user) {
		UserDto dto = new UserDto();

		dto.setId(user.getId());
		dto.setLogin(user.getLogin());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setUserRole(user.getUserRole());
		dto.setEmail(user.getEmail());

		return dto;
	}

	public User toUserEntity(UserDto dto) {
		User user = new User();

		user.setId(dto.getId());
		user.setLogin(dto.getLogin());
		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		user.setUserRole(dto.getUserRole());
		user.setEmail(dto.getEmail());

		if (dto.getPassword() != null && dto.getPassword() != "") {
			user.setPassword(
					BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt()));
		}

		if (Objects.nonNull(dto.getNotices()) && !dto.getNotices().isEmpty()) {
			user.getNotices().stream();
			dto.getNotices().stream().filter(Objects::nonNull)
					.forEach(el -> user.getNotices()
							.add(noticeRepository.getOne(el.getId())));
		}

		return user;
	}

	public NoticeDto toNoticeDto(Notice notice) {
		NoticeDto dto = new NoticeDto();

		dto.setId(notice.getId());
		dto.setSubject(notice.getSubject());
		dto.setContent(notice.getContent());
		dto.setCreated(notice.getCreated());
		dto.setIsActive(notice.getIsActive());
		if (notice.getCategories() != null
				&& !notice.getCategories().isEmpty()) {
			dto.getCategories().clear();
			notice.getCategories().stream().filter(Objects::nonNull)
					.map(this::toCategoryDto)
					.forEachOrdered(el -> dto.getCategories().add(el));

		}

		dto.setOwner(toSimpleUserDto(notice.getOwner()));

		return dto;
	}

	public Notice toNoticeEntity(NoticeDto dto) throws EntityNotFoundException {
		Notice notice;
		if (dto.getId() != null) {
			notice = noticeRepository.getOne(dto.getId());

		} else {
			notice = new Notice();

		}
		notice.setId(dto.getId());
		notice.setSubject(dto.getSubject());
		notice.setContent(dto.getContent());
		notice.setIsActive(dto.getIsActive());

		if (dto.getCategories() != null && !dto.getCategories().isEmpty()) {
			notice.getCategories().clear();
			notice.setCategories(dto.getCategories().stream()
					.filter(Objects::nonNull).map(this::toCategoryEntity)
					.collect(Collectors.toSet()));
		}

		notice.setOwner(userRepository.getOne(dto.getOwner().getId()));

		return notice;
	}

	public CategoryDto toCategoryDto(Category category) {
		CategoryDto dto = new CategoryDto();

		dto.setId(category.getId());
		dto.setName(category.getName());
		dto.setIsActice(category.getIsActice());

		return dto;
	}

	public Category toCategoryEntity(CategoryDto dto) {
		Category category = new Category();

		if (dto.getId() != null) {
			category = categoryRepository.getOne(dto.getId());
		}
		if (category == null) {
			category = new Category();
		}

		category.setId(dto.getId());
		category.setName(dto.getName());
		category.setIsActice(dto.getIsActice());

		return category;
	}

	public ImageDto toImageDto(Image image) {
		ImageDto dto = new ImageDto();

		dto.setId(image.getId());
		dto.setImageName(image.getImageName());
		dto.setImageFileName(image.getImageFileName());
		dto.setImage(image.getImage());

		return dto;
	}
	public Image toImageEntity(ImageDto dto) {
		Image image = new Image();

		image.setId(dto.getId());
		image.setImageName(dto.getImageName());
		image.setImageFileName(dto.getImageFileName());
		image.setImage(dto.getImage());

		return image;
	}

	public NoticeCommentDto toNoticeCommentDto(NoticeComment comment) {
		NoticeCommentDto dto = new NoticeCommentDto();

		dto.setId(comment.getId());
		dto.setCreated(comment.getCreated());
		dto.setContent(comment.getContent());
		if (Objects.nonNull(comment.getAuthor())) {
			dto.setAuthor(toSimpleUserDto(comment.getAuthor()));

		}
		dto.setNotice(toNoticeDto(comment.getNotice()));

		return dto;
	}

	public NoticeComment toNoticeCommentEntity(NoticeCommentDto dto)
			throws EntityNotFoundException {

		NoticeComment comment;

		if (dto.getId() != null) {
			comment = noticeCommentRepository.getOne(dto.getId());
		} else {
			comment = new NoticeComment();
		}

		comment.setId(dto.getId());
		comment.setContent(dto.getContent());
		if (Objects.nonNull(dto.getAuthor())) {
			comment.setAuthor(userRepository.getOne(dto.getAuthor().getId()));
		}
		if (Objects.nonNull(dto.getNotice())) {
			comment.setNotice(noticeRepository.getOne(dto.getNotice().getId()));

		}

		return comment;

	}

}
