package pl.coderslab.Commons.Result;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result {

	private String code;
	private String message;
	private List<String> errors = new ArrayList<>();
	private Object data;

	public Result(String code) {
		this.code = code;
	}

	public static Result ok(Object data) {
		Result result = new Result("200");
		result.setMessage("OK");
		result.setData(data);
		return result;
	}
	public static Result ok(String message) {
		Result result = new Result("200");
		result.setMessage(message);
		return result;
	}

	public static Result error(String error) {
		Result result = new Result("500");
		result.setMessage("ERROR");
		result.getErrors().add(error);
		return result;
	}

	public static Result error(String error, Object data) {
		Result result = new Result("500");
		result.setMessage("ERROR");
		result.getErrors().add(error);
		result.setData(data);
		return result;
	}
	public static Result error(String message, List<String> errors,
			Object data) {
		Result result = new Result("500");
		result.setMessage(message);
		result.getErrors().addAll(errors);
		result.setData(data);
		return result;
	}
}
