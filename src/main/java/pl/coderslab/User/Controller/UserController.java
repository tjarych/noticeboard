package pl.coderslab.User.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.coderslab.Commons.ENUM.UserRole;
import pl.coderslab.Commons.ErrorsUtil.ErrorsUtil;
import pl.coderslab.Commons.Result.Result;
import pl.coderslab.User.Dto.UserDto;
import pl.coderslab.User.Service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/create")
	public List<UserDto> generateData() {
		UserDto dto1 = new UserDto();

		dto1.setFirstName("Jan");
		dto1.setLastName("Kowalski");
		dto1.setLogin("kowal");
		dto1.setEmail("kowal@wp.pl");
		dto1.setPassword("kowal");
		dto1.setUserRole(UserRole.ADMIN);

		UserDto dto2 = new UserDto();

		dto2.setFirstName("Adam");
		dto2.setLastName("Nowak");
		dto2.setLogin("nowak");
		dto2.setEmail("nowak@wp.pl");
		dto2.setPassword("nowak");
		dto2.setUserRole(UserRole.USER);

		UserDto dto3 = new UserDto();

		dto3.setFirstName("Łukasz");
		dto3.setLastName("Andrzejczak");
		dto3.setLogin("andrzej");
		dto3.setEmail("andrzej@wp.pl");
		dto3.setPassword("andrzej");
		dto3.setUserRole(UserRole.GUEST);

		userService.save(dto1);
		userService.save(dto2);
		userService.save(dto3);

		return userService.getAll();
	}

	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getAllUsers() {

		return Result.ok(userService.getAll());
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result getUserById(@PathVariable("id") Long id) {
		return Result.ok(userService.findById(id));
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result addNewBook(@Valid @RequestBody UserDto dto,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
					bindingResult.getFieldErrors()), dto);
		}

		return Result.ok(userService.save(dto));
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Result updateUser(@Valid @RequestBody UserDto dto,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return Result.error(ErrorsUtil.errorsToStringFromFieldErrors(
					bindingResult.getFieldErrors()), dto);

		}

		return Result.ok(userService.save(dto));
	}

	@DeleteMapping("/{id}")
	public Result deleteUser(@PathVariable("id") Long id) {
		try {
			userService.deleteFromDb(id);
			return Result.ok("User has been delete");
		} catch (Exception e) {
			return Result.error("Cannot delete User of given ID");
		}
	}

}
