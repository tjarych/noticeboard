package pl.coderslab.User.Dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.coderslab.Commons.ENUM.UserRole;
import pl.coderslab.Notice.dto.NoticeDto;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDto {

	private Long id;

	@NotBlank(message = "Login is required")
	private String login;

	@NotBlank
	private String password;

	@NotBlank
	private String firstName;

	@NotBlank
	private String lastName;

	@Email
	private String email;

	private UserRole userRole;

	private Set<NoticeDto> notices = new HashSet<>();

}
