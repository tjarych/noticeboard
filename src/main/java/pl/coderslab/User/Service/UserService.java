package pl.coderslab.User.Service;

import pl.coderslab.Commons.Service.BaseCrudService;
import pl.coderslab.User.Dto.UserDto;

public interface UserService extends BaseCrudService<UserDto, Long> {

}
