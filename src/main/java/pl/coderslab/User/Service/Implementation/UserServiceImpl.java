package pl.coderslab.User.Service.Implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.coderslab.Commons.DtoAndEntityConverter.DtoAndEntityConverter;
import pl.coderslab.User.Domain.User;
import pl.coderslab.User.Dto.UserDto;
import pl.coderslab.User.Repository.UserRepository;
import pl.coderslab.User.Service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	private final DtoAndEntityConverter converter;

	@Autowired
	public UserServiceImpl(UserRepository userRepository,
			DtoAndEntityConverter converter) {
		this.userRepository = userRepository;
		this.converter = converter;
	}

	@Override
	public UserDto findById(Long id) {
		return converter.toUserDto(userRepository.getOne(id));
	}

	@Override
	public UserDto save(UserDto dto) {
		return converter
				.toUserDto(userRepository.save(converter.toUserEntity(dto)));
	}

	@Override
	public void deleteFromDb(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public List<UserDto> getAll() {
		return toUserDtoCollection(userRepository.findAll());
	}

	private List<UserDto> toUserDtoCollection(List<User> list) {
		return list.stream().map(converter::toUserDto)
				.collect(Collectors.toList());

	}

}
