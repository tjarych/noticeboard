package pl.coderslab.Image.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ImageDto {

	private Long id;

	@NotBlank
	private String imageName;

	private String imageFileName;

	@NotNull(message = "Image has not been attached")
	private byte[] image;

}
