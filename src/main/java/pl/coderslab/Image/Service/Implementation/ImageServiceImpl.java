package pl.coderslab.Image.Service.Implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.coderslab.Commons.DtoAndEntityConverter.DtoAndEntityConverter;
import pl.coderslab.Image.Repository.ImageRepository;
import pl.coderslab.Image.Service.ImageService;
import pl.coderslab.Image.domain.Image;
import pl.coderslab.Image.dto.ImageDto;

@Service
public class ImageServiceImpl implements ImageService {

	private final ImageRepository imageRepository;
	private final DtoAndEntityConverter converter;

	@Autowired
	public ImageServiceImpl(ImageRepository imageRepository,
			DtoAndEntityConverter converter) {
		this.imageRepository = imageRepository;
		this.converter = converter;
	}

	@Override
	public ImageDto findById(Long id) {
		return converter.toImageDto(imageRepository.getOne(id));
	}

	@Override
	public ImageDto save(ImageDto dto) {
		return converter
				.toImageDto(imageRepository.save(converter.toImageEntity(dto)));
	}

	@Override
	public void deleteFromDb(Long id) {
		imageRepository.deleteById(id);
	}

	@Override
	public List<ImageDto> getAll() {
		return toImageDtoCollection(imageRepository.findAll());
	}

	private List<ImageDto> toImageDtoCollection(List<Image> list) {
		return list.stream().map(converter::toImageDto)
				.collect(Collectors.toList());

	}

}
