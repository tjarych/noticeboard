package pl.coderslab.Image.Service;

import pl.coderslab.Commons.Service.BaseCrudService;
import pl.coderslab.Image.dto.ImageDto;

public interface ImageService extends BaseCrudService<ImageDto, Long> {

}
