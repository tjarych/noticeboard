package pl.coderslab.Image.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.coderslab.Image.domain.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

}
