package pl.coderslab.Image.Controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.coderslab.Commons.Result.Result;
import pl.coderslab.Image.Service.ImageService;
import pl.coderslab.Image.dto.ImageDto;

@RestController
@RequestMapping("/image")
public class ImageController {

	private final ImageService imageService;

	@Autowired
	public ImageController(ImageService imageService) {
		this.imageService = imageService;
	}

	@GetMapping(path = "/create", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result createImageInDb() {
		ImageDto imageToSave = new ImageDto();

		File file = new File(
				"/home/tj/workspace/Noticeboard/src/main/resources/static/java.jpeg");
		byte[] imageInByte = new byte[(int) file.length()];

		try (FileInputStream inputStream = new FileInputStream(file)) {
			inputStream.read(imageInByte);

			imageToSave.setImage(imageInByte);
			imageToSave.setImageFileName("java.jpg");
			imageToSave.setImageName("Funny java");

			imageService.save(imageToSave);

		} catch (IOException e) {
			return Result.error("File not found");
		}
		return Result.ok(imageToSave);
	}

	@GetMapping(path = "/read", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result readImageFromDb() {

		ImageDto dto = imageService.findById(1L);

		File file = new File("javaFromDB.jpeg");
		byte[] imageInByte = dto.getImage();

		try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			fileOutputStream.write(imageInByte);
		} catch (IOException e) {
			return Result.error("File not found");
		}
		return Result.ok("Image has been read");
	}

	@GetMapping(path = "/all", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result getAllImages() {
		return Result.ok(imageService.getAll());
	}

	@GetMapping(path = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result getImageById(@PathVariable("id") Long id) {
		return Result.ok(imageService.findById(id));
	}

	@PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public Result saveImage(@Valid @RequestBody ImageDto dto,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return Result.error("Image couldn't be save");
		}
		imageService.save(dto);
		return Result.ok("Image has been saved");
	}

	@PutMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public Result editImage(@Valid @RequestBody ImageDto dto,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return Result.error("Image couldn't be save");
		}
		imageService.save(dto);
		return Result.ok("Image has been saved");
	}

	@DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	public Result deleteImage(@PathVariable("id") Long id) {
		try {
			imageService.deleteFromDb(id);
			return Result.ok("Image has been deleted");
		} catch (Exception e) {
			return Result.error("Image hasn't been deleted");
		}
	}

}
